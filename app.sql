/*
Navicat MySQL Data Transfer

Source Server         : app
Source Server Version : 50018
Source Host           : localhost:3306
Source Database       : app

Target Server Type    : MYSQL
Target Server Version : 50018
File Encoding         : 65001

Date: 2014-05-24 20:32:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `user_id` int(11) NOT NULL auto_increment,
  `login` text NOT NULL,
  `password` text NOT NULL,
  `access_level` enum('ROLE_ADMIN','ROLE_USER','ROLE_GUEST') NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES ('1', 'user', 'user', 'ROLE_USER', '');
INSERT INTO `accounts` VALUES ('2', 'artemi4', 'program', 'ROLE_ADMIN', '');

-- ----------------------------
-- Table structure for `basket`
-- ----------------------------
DROP TABLE IF EXISTS `basket`;
CREATE TABLE `basket` (
  `record_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY  (`record_id`),
  KEY `FKACC7B9C63D43702C` (`user_id`),
  KEY `FKACC7B9C636EE87C8` (`product_id`),
  CONSTRAINT `FKACC7B9C636EE87C8` FOREIGN KEY (`product_id`) REFERENCES `product` (`PRODUCT_ID`),
  CONSTRAINT `FKACC7B9C63D43702C` FOREIGN KEY (`user_id`) REFERENCES `accounts` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of basket
-- ----------------------------

-- ----------------------------
-- Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `CATEGORY_ID` int(11) NOT NULL auto_increment,
  `CATEGORY_NAME` text,
  PRIMARY KEY  (`CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'Фрукты');
INSERT INTO `category` VALUES ('2', 'Овощи');
INSERT INTO `category` VALUES ('3', 'Цитрус');
INSERT INTO `category` VALUES ('4', 'Ягоды');
INSERT INTO `category` VALUES ('5', 'Электроника');
INSERT INTO `category` VALUES ('6', 'Быт. техника');
INSERT INTO `category` VALUES ('7', 'Цифровая электроника');
INSERT INTO `category` VALUES ('8', 'Программное обеспечение');
INSERT INTO `category` VALUES ('9', 'Мобильная связь');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `PRODUCT_ID` int(11) NOT NULL auto_increment,
  `PRODUCT_NAME` text,
  `PRODUCT_COUNT` int(11) default NULL,
  `PRODUCT_MADE` text,
  PRIMARY KEY  (`PRODUCT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', 'Яблоко', '55', 'Украина');
INSERT INTO `product` VALUES ('2', 'Груша', '65', 'Украина');
INSERT INTO `product` VALUES ('3', 'Помидор', '32', 'Украина');
INSERT INTO `product` VALUES ('4', 'Огурец', '78', 'Польша');
INSERT INTO `product` VALUES ('5', 'Арбуз', '67', 'Украина');
INSERT INTO `product` VALUES ('6', 'Малина', '98', 'Турция');
INSERT INTO `product` VALUES ('7', 'Клубника', '178', 'Турция');
INSERT INTO `product` VALUES ('8', 'Черника', '76', 'Украина');
INSERT INTO `product` VALUES ('9', 'Картофель', '235', 'Беларусь');
INSERT INTO `product` VALUES ('10', 'Лук', '345', 'Украина');
INSERT INTO `product` VALUES ('11', 'Капуста', '135', 'Россия');
INSERT INTO `product` VALUES ('12', 'Кукуруза', '123', 'Украина');
INSERT INTO `product` VALUES ('13', 'Слива', '23', 'Украина');
INSERT INTO `product` VALUES ('14', 'Часнок', '145', 'Украина');
INSERT INTO `product` VALUES ('15', 'Буряк', '367', 'Украина');
INSERT INTO `product` VALUES ('16', 'Банан', '100', 'Египет');
INSERT INTO `product` VALUES ('17', 'Апельсин', '123', 'Египет');
INSERT INTO `product` VALUES ('18', 'Грейпфрут', '32', 'Пакистан');
INSERT INTO `product` VALUES ('19', 'Ананас', '45', 'Египет');

-- ----------------------------
-- Table structure for `prod_cat`
-- ----------------------------
DROP TABLE IF EXISTS `prod_cat`;
CREATE TABLE `prod_cat` (
  `CATEGORY_ID` int(11) default NULL,
  `PRODUCT_ID` int(11) default NULL,
  KEY `FKC421CEEE6BAB423F` (`CATEGORY_ID`),
  KEY `FKC421CEEE5EB80175` (`PRODUCT_ID`),
  CONSTRAINT `FKC421CEEE5EB80175` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `product` (`PRODUCT_ID`),
  CONSTRAINT `FKC421CEEE6BAB423F` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `category` (`CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prod_cat
-- ----------------------------
INSERT INTO `prod_cat` VALUES ('1', '1');
INSERT INTO `prod_cat` VALUES ('1', '2');
INSERT INTO `prod_cat` VALUES ('2', '3');
INSERT INTO `prod_cat` VALUES ('2', '4');
INSERT INTO `prod_cat` VALUES ('3', '5');
INSERT INTO `prod_cat` VALUES ('3', '6');
INSERT INTO `prod_cat` VALUES ('4', '7');
INSERT INTO `prod_cat` VALUES ('4', '8');
INSERT INTO `prod_cat` VALUES ('5', '9');
INSERT INTO `prod_cat` VALUES ('5', '10');
INSERT INTO `prod_cat` VALUES ('6', '13');
INSERT INTO `prod_cat` VALUES ('6', '12');
INSERT INTO `prod_cat` VALUES ('7', '15');
INSERT INTO `prod_cat` VALUES ('7', '14');
INSERT INTO `prod_cat` VALUES ('8', '17');
INSERT INTO `prod_cat` VALUES ('8', '16');
INSERT INTO `prod_cat` VALUES ('9', '19');
INSERT INTO `prod_cat` VALUES ('9', '18');
