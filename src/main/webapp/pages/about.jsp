<%--
  Created by IntelliJ IDEA.
  User: Артем
  Date: 16.05.14
  Time: 9:05
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page pageEncoding="utf-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>О продукте</title>
    <link href="/css/im.css" type="text/css" rel="stylesheet"/>
    <link href="/css/menu.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<sec:authorize access="isAuthenticated()">
    <%@ include file="logined/mainMenu.jsp" %>
</sec:authorize>
 Этот сайт был разработан как пример для создания более сложных систем.
</body>
</html>