<%--
  Created by IntelliJ IDEA.
  User: Артем
  Date: 16.05.14
  Time: 10:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Регистрация нового юзера</title>
    <link href="/css/im.css" type="text/css" rel="stylesheet"/>
</head>
<body>

<form:form action="/register"  method="POST" commandName="user" modelAttribute="user">
    <form:errors path="*" cssClass="errorblock" element="div"/>
    <table>
        <tr>
            <td><font color="red">Логин:</font></td>
            <td><form:input path="login"/></td>
        </tr>
        <tr>
            <td><font color="red">Пасворд:</font></td>
            <td>
                <form:password path="password"/>
            </td>

        </tr>
        <tr>
            <td><input name="submit" type="submit" value="Зарегиться" /></td>
        </tr>
    </table>
</form:form>
</body>
</html>