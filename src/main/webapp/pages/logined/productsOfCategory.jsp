<%--
  Created by IntelliJ IDEA.
  User: TEMA
  Date: 08.04.13
  Time: 1:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.jar.Attributes" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<p align="center" id="white">Все продукты категории: ${categoryView.categoryName}</p>
<c:if test="${products_list!=null}">

    <table border="1" cellpadding="5" cellspacing="5" align="center" id="white">
        <tr>
            <td>№</td>
            <td>Название продукта</td>
            <td>В наличии</td>
            <td>Страна изготовитель</td>
            <%--<td><font color="white">категории продукта</font></td> --%>
        </tr>
        <c:forEach var="product" items="${products_list}" varStatus="stat">
            <tr>
                <td>${stat.index+1}</td>
                <td>${product.productName}</td>
                <td>${product.productCount}</td>
                <td>${product.productMade}</td>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                <td>
                    <a href="<c:url value="/logined/product/delete?id=${product.productId}" />">delete</a>
                </td>
                </sec:authorize>
            </tr>
        </c:forEach>
    </table>

</c:if>