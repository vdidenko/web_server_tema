<%--
  Created by IntelliJ IDEA.
  User: TEMA
  Date: 05.04.13
  Time: 19:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="hasRole('ROLE_ADMIN')">
<table align="center" id="white">
    <tr>

        <td align="left">Название категории:</td>
    </tr>
    <tr>
        <form:form action="/logined/category/add" method="POST" commandName="category" modelAttribute="category">
        <form:errors path="*" cssClass="errorblock" element="div"/>
        <td align="left">

                <form:input path="categoryName" type="text"/>
                <input type="submit" value="Сохранить категорию">
            </form:form>
        </td>
    </tr>
</table>
</sec:authorize>