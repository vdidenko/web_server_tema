<%--
  Created by IntelliJ IDEA.
  User: TEMA
  Date: 05.04.13
  Time: 19:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.jar.Attributes" %>
<%@ page import="java.util.List" %>
<%@ page import="com.model.Product" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.controller.BasketController" %>
<%@ page import="com.model.Basket" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%-- <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:if test="${product_list!=null}">

    <table border="1" cellpadding="5" cellspacing="5" align="center" id="white">
        <tr>
            <td>№</td>
            <td>Название продукта</td>
            <td>В наличии</td>
            <td>Страна изготовитель</td>
            <td>Категории продукта</td>

        </tr>

        <c:forEach var="prod" items="${product_list}" varStatus="stat">

            <tr>

                <td>${10*(pageN-1)+stat.index+1}</td>
                <td>
                    <sec:authorize access="hasRole('ROLE_USER')">
                    <a href="<c:url value="/logined/fullProductInf?id=${prod.productId}"/>">${prod.productName}</a>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        ${prod.productName}
                    </sec:authorize>
                </td>
                <td>${prod.productCount}</td>
                <td>${prod.productMade}</td>
                <td>
                    <c:forEach var="categories" items="${prod.categories}" varStatus="status">
                        ${categories.categoryName}
                        <c:if test="${!status.last}">
                            ,&nbsp;
                            <c:if test="${(status.index+1)%3==0}">
                                <br>
                            </c:if>
                        </c:if>
                    </c:forEach>
                </td>

                <sec:authorize access="hasRole('ROLE_ADMIN')">
                <td>
                    <a href="<c:url value="/logined/product/delete?id=${prod.productId}" />">delete</a>
                </td>
                </sec:authorize>

            </tr>
        </c:forEach>
    </table>
</c:if>
<center>
    <c:if test="${product_list!=null}">page:</c:if>
    <c:forEach var="page" items="${pages}" varStatus="status">
        <a href="/logined/viewer/products?pageNumber=${status.index+1}">${status.index+1}</a>
        &nbsp;&nbsp;&nbsp;
    </c:forEach>
</center>
