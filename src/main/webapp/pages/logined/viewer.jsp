<%--
  Created by IntelliJ IDEA.
  User: HOME
  Date: 30.03.13
  Time: 15:41
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page pageEncoding="utf-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Проэкт "Буря в пустыне"</title>
    <link href="/css/im.css" type="text/css" rel="stylesheet"/>
    <link href="/css/menu.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<sec:authorize access="isAuthenticated()">
    <%@ include file="mainMenu.jsp" %>
    <%@include file="displayProduct.jsp" %>
    <%@include file="displayCategory.jsp" %>
    <%@include file="viewBasket.jsp"%>
</sec:authorize>
</body>
</html>