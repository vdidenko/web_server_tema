<%--
  Created by IntelliJ IDEA.
  User: HOME
  Date: 30.03.13
  Time: 15:41
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Проэкт "Буря в пустыне"</title>
    <link href="/css/im.css" type="text/css" rel="stylesheet"/>
    <link href="/css/menu.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <h1 align="CENTER"><font color="fuchsia">Категория успешно удалена</font></h1>
</sec:authorize>
<%@ include file="mainMenu.jsp" %>
<%@include file="displayProduct.jsp" %>
<%@include file="displayCategory.jsp" %>
</body>
</html>