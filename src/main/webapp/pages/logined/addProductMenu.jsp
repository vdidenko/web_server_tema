<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: TEMA
  Date: 05.04.13
  Time: 19:36
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<sec:authorize access="hasRole('ROLE_ADMIN')">
<form:form action="/logined/product/add" method="POST" commandName="product" modelAttribute="product">
<table align="center" id="white">
    <c:if test="${category_list!=null}">
<tr>
    <td>Название продукта:</td>
    <td>Сколько в наличии:</td>
    <td>Страна изготовитель:</td>
    <%--<td><font color="white">категории продукта</font></td> --%>
</tr>
    </c:if>

<tr>

    <form:errors path="*" cssClass="errorblock" element="div"/>
    <c:if test="${category_list!=null}">
    <td>
        <form:input path="productName"/>

    </td>
    <td>
        <input name="productCount" pattern="[0-9]{1,4}"/>

    </td>
    <td>
        <form:input path="productMade"/>

    </td>
    <td>
        <input type="submit" value="Сохранить продукт">
    </td>
    </c:if>
    </tr>


    </table>
    <c:if test="${category_list!=null}">
        <table border="1" cellpadding="5" cellspacing="5" align="center" id="white">
            <tr>
                <td>Название категории</td>
                <td>выберите категорию</td>
            </tr>
            <c:forEach var="category" items="${category_list}">
                <tr>

                    <td>
                        <a href="<c:url value="/logined/fullCategoryInf?id=${category.categoryId}"/>">${category.categoryName}</a>

                    </td>
                    <td>
                        <input type="checkbox" value="${category.categoryId}" name="idList[]" checked="checked">
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</form:form>
</sec:authorize>
