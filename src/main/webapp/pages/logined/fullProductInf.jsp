<%--
  Created by IntelliJ IDEA.
  User: TEMA
  Date: 05.04.13
  Time: 19:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.jar.Attributes" %>
<%@ page import="java.util.List" %>
<%@ page import="com.model.Product" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.controller.BasketController" %>
<%@ page import="com.model.Basket" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%-- <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Проэкт "Буря в пустыне"</title>
    <link href="/css/im.css" type="text/css" rel="stylesheet"/>
    <link href="/css/menu.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<%@ include file="mainMenu.jsp" %>
<sec:authorize access="isAuthenticated()">
<c:if test="${product!=null}">
    <form:form action="/logined/addBasket" method="post" commandName="basket" modelAttribute="basket">

    <table border="1" cellpadding="5" cellspacing="5" align="center" id="white">
        <tr>
            <form:errors path="*" cssClass="errorblock" element="div"/>
            <td>Название продукта</td>
            <td>В наличии</td>
            <td>Страна изготовитель</td>
            <td>Заказ</td>
        </tr>
        <tr>
            <td>
                ${product.productName}
            </td>
            <td>
                ${product.productCount}

            </td>
            <td>
                ${product.productMade}
            </td>
            <td>
                <input name="count" pattern="[0-9]{1,4}"/>
            </td>
            <td>
                <input type="submit" value="В корзину">
            </td>
        </tr>
    </table>
    </form:form>
</c:if>
</sec:authorize>
</body>
</html>