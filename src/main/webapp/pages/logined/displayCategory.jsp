<%--
  Created by IntelliJ IDEA.
  User: TEMA
  Date: 05.04.13
  Time: 19:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <c:if test="${category_list!=null}">
        <table border="1"  cellpadding="5" cellspacing="5" align="center" id="white">
            <tr>
                <td>№</td>
                <td>Название категории</td>
            </tr>
            <c:forEach var="category" items="${category_list}" varStatus="stat">
                <tr>
                    <td>
                     ${stat.index+1}
                    </td>

                    <td>
                        <a href="<c:url value="/logined/fullCategoryInf?id=${category.categoryId}"/>">${category.categoryName}</a>

                    </td>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <td>
                        <a href="<c:url value="/logined/category/delete?id=${category.categoryId}" />">delete</a>
                    </td>
                    </sec:authorize>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</sec:authorize>