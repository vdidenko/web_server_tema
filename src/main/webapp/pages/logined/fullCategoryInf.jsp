<%--
  Created by IntelliJ IDEA.
  User: TEMA
  Date: 08.04.13
  Time: 1:19
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>Проэкт "Буря в пустыне"</title>
    <link href="/css/im.css" type="text/css" rel="stylesheet"/>
    <link href="/css/menu.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<sec:authorize access="isAuthenticated()">
    <%@ include file="mainMenu.jsp" %>
    <%@include file="productsOfCategory.jsp" %>
</sec:authorize>
</body>
</html>