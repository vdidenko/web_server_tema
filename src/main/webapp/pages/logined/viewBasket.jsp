<%--
  Created by IntelliJ IDEA.
  User: TEMA
  Date: 05.04.13
  Time: 19:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.jar.Attributes" %>
<%@ page import="java.util.List" %>
<%@ page import="com.model.Product" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%-- <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
        <c:if test="${basketList!=null&&product_list==null}">
            <table border="1" cellpadding="5" cellspacing="5" align="center" id="white">
                <tr>
                    <td>№</td>
                    <td>Название продукта</td>
                    <td>Заказано</td>
                </tr>

                <c:forEach var="bask" items="${basketList}" varStatus="stat">

                    <tr>
                        <td>${stat.index+1}</td>
                        <td>${bask.product.productName}</td>
                        <td>${bask.count}</td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
</sec:authorize>