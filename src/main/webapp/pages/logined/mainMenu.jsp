<%--
  Created by IntelliJ IDEA.
  User: TEMA
  Date: 05.04.13
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url value="/j_spring_security_logout" var="logoutUrl"/>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <div id="navigation">
        <ul>
            <li>
                <a href="#">Просмотреть</a>
                <ul>
                    <li><a href="/logined/viewer/categories">Категории</a></li>
                    <li><a href="/logined/viewer/products?pageNumber=1">Продукты</a></li>
                    <li><a href="/logined/displayUsers">Юзеров</a></li>
                </ul>
            </li>

            <li>
                <a href="#">Добавить</a>
                <ul>
                    <li><a href="/logined/product">Продукты</a></li>
                    <li><a href="/logined/category">Категории</a></li>
                </ul>
            </li>
            <li>
                <a href="/index">Главная</a>
                <ul>
                    <li>
                        <a href="/about">О сайте</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="${logoutUrl}">Выйти</a>
            </li>
        </ul>
    </div>

</sec:authorize>
<sec:authorize access="hasRole('ROLE_USER')">
    <div id="navigation">
        <ul>
            <li>
                <a href="#">Просмотреть</a>
                <ul>
                    <li><a href="/logined/viewer/categories">Категории</a></li>
                    <li><a href="/logined/viewer/products?pageNumber=1">Продукты</a></li>
                    <li><a href="/logined/viewer/basket">Корзину</a></li>
                </ul>
            </li>
            <li>
                <a href="/index">Главная</a>
                <ul>
                    <li>
                        <a href="/about">О сайте</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="${logoutUrl}">Выйти</a>
            </li>
        </ul>
    </div>
</sec:authorize>