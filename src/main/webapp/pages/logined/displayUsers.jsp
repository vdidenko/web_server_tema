<%--
  Created by IntelliJ IDEA.
  User: TEMA
  Date: 05.04.13
  Time: 19:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%-- <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Проэкт "Буря в пустыне"</title>
    <link href="/css/im.css" type="text/css" rel="stylesheet"/>
    <link href="/css/menu.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<%@ include file="mainMenu.jsp" %>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <c:if test="${userList!=null}">

        <table border="1" cellpadding="5" cellspacing="5" align="center" id="white">
            <tr>
                <td>№</td>
                <td>Логин</td>
                <td>Статус</td>
            </tr>

            <c:forEach var="us" items="${userList}" varStatus="stat">

                <tr>

                    <td>${stat.index+1}</td>
                    <td>${us.login}</td>
                    <td>
                        <c:if test="${us.enabled}">
                            <a href="<c:url value="/logined/disableUser?id=${us.login}"/>">включён</a>
                        </c:if>
                        <c:if test="${!us.enabled}">
                            <a href="<c:url value="/logined/disableUser?id=${us.login}"/>">выключен</a>
                        </c:if>
                    </td>
                    <td><a href="<c:url value="/logined/deleteUser?id=${us.login}"/>">Delete</a></td>

                </tr>
            </c:forEach>
        </table>
    </c:if>
</sec:authorize>
</body>
</html>