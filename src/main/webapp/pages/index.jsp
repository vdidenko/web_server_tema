<%--
  Created by IntelliJ IDEA.
  User: HOME
  Date: 30.03.13
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>Web-магазин</title>
    <link href="/css/im.css" type="text/css" rel="stylesheet"/>

</head>
<body>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <a href="/logined/viewer" title="TEST">
        Войти как администратор
    </a>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_USER')">
    <a href="/logined/viewer" title="TEST">
        Войти как
        <sec:authentication property="principal.username"/>
    </a>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_GUEST')">
    <a href="/login" title="TEST">
        Войти как гость
    </a>
</sec:authorize>
</body>
</html>