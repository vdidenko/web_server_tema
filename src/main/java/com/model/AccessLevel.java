package com.model;

/**
 * Created with IntelliJ IDEA.
 * User: Артем
 * Date: 16.05.14
 * Time: 14:02
 * By Orion
 */
public enum AccessLevel {
     ROLE_ADMIN, ROLE_USER, ROLE_GUEST;
}
