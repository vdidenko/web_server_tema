package com.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Orion
 * Date: 09.03.13
 * Time: 18:03
 * That which we call a rose by any other name would smell as sweet.
 *
 * @author Artem FedoroFF
 */
@Entity
@Table(name = "category")

public class Category implements Serializable {
    private Integer categoryId;
    private String categoryName;
    private List<Product> products;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CATEGORY_ID")
    public Integer getCategoryId() {
        return categoryId;
    }

    private void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Column(name = "CATEGORY_NAME")
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @ManyToMany(targetEntity = Product.class, cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "prod_cat",
            joinColumns = {@JoinColumn(
                    name = "CATEGORY_ID",
                    referencedColumnName = "CATEGORY_ID")},
            inverseJoinColumns = {@JoinColumn(
                    name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")})
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}
