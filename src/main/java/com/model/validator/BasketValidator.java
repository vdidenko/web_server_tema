package com.model.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created with IntelliJ IDEA.
 * User: TEMA
 * Date: 05.06.14
 * Time: 20:21
 * that which we call a rose by any other name would smell as sweet
 */
@Component
public class BasketValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return BasketValidator.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "count", "error");
    }
}
