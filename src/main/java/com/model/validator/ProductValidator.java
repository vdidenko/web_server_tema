package com.model.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * User: HOME
 * Date: 06.04.13
 * Time: 16:00
 */
@Component
public class ProductValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return ProductValidator.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productName", "error");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productCount", "error");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productMade", "error");
    }
}
