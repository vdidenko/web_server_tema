package com.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Orion
 * Date: 09.03.13
 * Time: 18:03
 * That which we call a rose by any other name would smell as sweet.
 *
 * @author Artem FedoroFF
 */
@Entity
@Table(name = "product")
public class Product implements Serializable {
    private Integer productId;
    private String productName;
    private List<Category> categories;
    private Integer productCount;
    private String productMade;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PRODUCT_ID")
    public Integer getProductId() {
        return productId;
    }

    private void setProductId(Integer productId) {
        this.productId = productId;
    }

    @Column(name = "PRODUCT_NAME")
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Column(name = "PRODUCT_COUNT")
    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    @Column(name = "PRODUCT_MADE")
    public String getProductMade() {
        return productMade;
    }

    public void setProductMade(String productMade) {
        this.productMade = productMade;
    }

    @ManyToMany(targetEntity = Category.class, cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinTable(
            name = "prod_cat",
            joinColumns = {@JoinColumn(
                    name = "PRODUCT_ID",
                    referencedColumnName = "PRODUCT_ID")},
            inverseJoinColumns = {@JoinColumn(
                    name = "CATEGORY_ID", referencedColumnName = "CATEGORY_ID")})
    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
