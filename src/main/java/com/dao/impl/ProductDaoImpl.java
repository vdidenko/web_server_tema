package com.dao.impl;

import com.SessionManager;
import com.dao.ProductDao;
import com.model.Product;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Orion
 * Date: 09.03.13
 * Time: 18:39
 * That which we call a rose by any other name would smell as sweet.
 *
 * @author Artem FedoroFF
 */
@Component
public class ProductDaoImpl extends SessionManager implements ProductDao {

    private static final Logger LOGGER= LoggerFactory.getLogger(ProductDaoImpl.class);
    @Transactional(readOnly = true)
    public Product getProductById(Integer id){
        Product product=null;
        Query query = null;
        try {
            query = getCurrentSession().createQuery("select product from Product as product " +
                    "left join fetch product.categories as  Category " +
                    "where product.productId=:prodId").setInteger("prodId", id);
            product=(Product) query.uniqueResult();
        } catch (Exception e) {
            LOGGER.error("Failure get product with fetch");
        }
        return product;
    }
    @Transactional(readOnly = true)
    public Product getOneProductById(Integer id){
        Product product=null;
        try {
            product=(Product)getCurrentSession().get(Product.class,id);
            if(null==product){
                product=(Product)getCurrentSession().load(Product.class,id);
            }

        } catch (Exception e) {
            LOGGER.error("Failure get one product by id");
        }
        return product;
    }
    @Transactional(readOnly = true)
    public List<Product> getSeveralProducts(Integer firstId,Integer lastId){
        List<Product> list = null;
        try {
            list=getCurrentSession().createQuery("select product from Product as product")
                    .setFirstResult(firstId)
                    .setMaxResults(lastId).list();

        }   catch (Exception e){
            LOGGER.error("Failure find records with: {} on {} ",firstId,lastId);
        }
        return list;
    }
    @Transactional(readOnly = true)
    @Override
    public List<Product> getSeveralProducts(Integer[] idList){
        //перегруженный метод getSeveralProducts выбирает продукты по айди с листа
        List list = null;
        try {
            list=getCurrentSession().createQuery("select product from Product as product where product.id IN :list")
                    .setParameterList("list",idList).list();

        }   catch (Exception e){
            LOGGER.error("Failure find records with: {} on {} ");
        }
        return list;
    }
    @Transactional
    public void saveProduct(Product product) throws HibernateException {
        try {
            getCurrentSession().save(product);
        } catch (Exception e) {
            LOGGER.error("Failure save product");
        }
    }
    @Transactional(readOnly = true)
    public List<Product> getAllProducts()throws HibernateException{
        return getCurrentSession().createQuery("from Product").list();
    }
    @Transactional
    public void removeProduct(Integer id){
        Product product=null;
        try{
        product=(Product)getCurrentSession().get(Product.class, id);
        if(null!=product){
            getCurrentSession().delete(product);
        }}catch (HibernateException e){
            LOGGER.error("Failure delete product");
        }

    }
    @Transactional
    public BigInteger getCountRecords(){
        return (BigInteger)getCurrentSession().createSQLQuery("select count(*) from product").uniqueResult();
    }

}
