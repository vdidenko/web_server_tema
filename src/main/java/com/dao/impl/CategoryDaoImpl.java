package com.dao.impl;

import com.SessionManager;
import com.dao.CategoryDao;
import com.model.Category;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Orion
 * Date: 09.03.13
 * Time: 18:38
 * That which we call a rose by any other name would smell as sweet.
 *
 * @author Artem FedoroFF
 */
@Component
public class CategoryDaoImpl extends SessionManager implements CategoryDao {
    private static final Logger LOGGER= LoggerFactory.getLogger(CategoryDaoImpl.class);

    @Transactional
    public Category getCategoryById(Integer id) throws HibernateException {

        Query query = null;
        try {
            query = getCurrentSession().createQuery("select category from Category as category " +
                    "left join fetch category.products as  Product " +
                    "where category.categoryId=:catId").setInteger("catId", id);

        } catch (Exception e) {
            LOGGER.error("Failure get category by id with fetch");
        }
        return (Category) query.uniqueResult();
    }
    @Transactional
    public Category getOneCategoryById(Integer id){

        Category category=null;
        try {
            category=(Category)getCurrentSession().get(Category.class,id);
            if(null==category){
                category=(Category)getCurrentSession().load(Category.class,id);
            }

        } catch (Exception e) {
            LOGGER.error("Failure get one category by id");
        }
        return category;
    }
    @Transactional
    public void saveCategory(Category category) throws HibernateException {

        try {
            getCurrentSession().save(category);

        } catch (Exception e) {
            LOGGER.error("Failure save category");
        }
    }

    @Transactional(readOnly = true)
    public List<Category> getAllCategories() throws HibernateException {
        return getCurrentSession().createQuery("from Category").list();
    }

    @Transactional
    public void removeCategory(Integer id) {
        Category category=null;
        try{
         category= (Category) getCurrentSession().get(Category.class, id);
        if (null != category) {
            getCurrentSession().delete(category);
        }
        }catch (HibernateException e){
            LOGGER.error("Failure get all category");
        }
    }
    @Transactional(readOnly = true)
    public List<Category> getSeveralCategories(Integer[] idList){
        List list = null;
        try {
            list=getCurrentSession().createQuery("select category from Category as category where category.id IN :list")
            .setParameterList("list",idList).list();

        }   catch (Exception e){
            LOGGER.error("Failure find records with: {} on {} ");
        }
        return list;
    }
}
