package com.dao.impl;

import com.SessionManager;
import com.dao.UserDao;
import com.model.AccessLevel;
import com.model.Basket;
import com.model.User;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Артем
 * Date: 16.05.14
 * Time: 14:10
 * By Orion
 */
@Component
public class UserDaoImpl extends SessionManager implements UserDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @Transactional
    public void saveUser(User user) throws HibernateException {
        try {
            getCurrentSession().saveOrUpdate(user);
        } catch (Exception e) {
            LOGGER.error("Failure save product");
        }

    }

    @Transactional
    public void removeUser(User user) throws HibernateException {
        try {
            if (null != user) {
                getCurrentSession().delete(user);
            }
        } catch (HibernateException e) {
            LOGGER.error("Failure delete product");
        }

    }

    @Transactional(readOnly = true)
    public List<User> getAllUsers() throws HibernateException {
        List list = null;
        try {
            list = getCurrentSession().createQuery("select user from User as user").list();

        } catch (Exception e) {
            LOGGER.error("Failure find records with: {} on {} ");
        }
        return list;
    }
    @Transactional(readOnly = true)
    public List<User> getJustUsers() throws HibernateException {
        List list = null;
        try {
            list = getCurrentSession().createQuery("select user from User as user" +
                    " where user.accessLevel=:role").setParameter("role", AccessLevel.ROLE_USER).list();

        } catch (Exception e) {
            LOGGER.error("Failure find records with: {} on {} ");
        }
        return list;
    }
    @Transactional(readOnly = true)
    public User getUserByLogin(String login) throws HibernateException {
        User user = null;
        Query query = null;
        try {
            query = getCurrentSession().createQuery("select user from User as user " +

                    "where user.login=:login").setString("login", login);
            user = (User) query.uniqueResult();
        } catch (Exception e) {
            LOGGER.error("Failure get product with fetch");
        }
        return user;
    }

    @Transactional(readOnly = true)
    public User getUserById(Integer id) {
        User user = null;
        Query query = null;
        try {
            query = getCurrentSession().createQuery("select user from User as user " +

                    "where user.userId=:userId").setInteger("userId", id);
            user = (User) query.uniqueResult();
        } catch (Exception e) {
            LOGGER.error("Failure get product with fetch");
        }
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Basket> getBasketOfUser(User user) throws HibernateException {
        Query query = null;
        try {
            query = getCurrentSession().createQuery("select basket from Basket as basket " +
                    "left join fetch basket.product as  product " +
                    "where basket.user=:user").setParameter("user", user);
        } catch (Exception e) {
            LOGGER.error("Failure get basketList of user: " + user.getLogin());
        }
        return query.list();
    }

    @Transactional
    @Override
    public void saveBasketOfUser(User user) throws HibernateException {
        ArrayList<Basket> inList = (ArrayList<Basket>) user.getBasketList();
        ArrayList<Basket> outList = (ArrayList<Basket>) getBasketOfUser(user);
        ArrayList<Basket> deleteList = new ArrayList<Basket>();
        for(Basket inBasket:inList) {
            if (inBasket.getCount() > 0) {
                for (Basket outBasket : outList) {
                    if (inBasket.getProduct().getProductName().equals(outBasket.getProduct().getProductName()) &&
                            inBasket.getProduct().getProductMade().equals(outBasket.getProduct().getProductMade())) {
                        inBasket.setCount(inBasket.getCount() + outBasket.getCount());
                        deleteList.add(outBasket);
                    }
                }
            }
        }
        int i=inList.size();
        int k=0;
        while (k<i){

            if(inList.get(k).getCount()<=0){
                inList.remove(inList.get(k));k--;i--;
            }
            k++;
        }
        try {
            for (Basket basket : deleteList) {
                getCurrentSession().delete(basket);
            }
        } catch (Exception e) {
            LOGGER.error("не удалось удалить с базы запись с корзины");
        }
        try {
            for (Basket basket : inList) {
                getCurrentSession().saveOrUpdate(basket);
            }
        } catch (Exception e) {
            LOGGER.error("Failure save item of basket of user: " + user.getLogin());
        }
    }
}
