package com.dao;

import com.model.Category;
import org.hibernate.HibernateException;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Orion
 * Date: 09.03.13
 * Time: 18:33
 * That which we call a rose by any other name would smell as sweet.
 *
 * @author Artem FedoroFF
 */
public interface CategoryDao {
    public Category getCategoryById(Integer id) throws HibernateException;

    public void saveCategory(Category category) throws HibernateException;

    public List getAllCategories() throws HibernateException;

    public void removeCategory(Integer id) throws HibernateException;

    public Category getOneCategoryById(Integer id);

    public List<Category> getSeveralCategories(Integer[] idList);
}
