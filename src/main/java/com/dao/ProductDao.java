package com.dao;

import com.model.Product;
import org.hibernate.HibernateException;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Orion
 * Date: 09.03.13
 * Time: 18:33
 * That which we call a rose by any other name would smell as sweet.
 *
 * @author Artem FedoroFF
 */
public interface ProductDao {
    public Product getProductById(Integer id);

    public void saveProduct(Product product) throws HibernateException;

    public List<Product> getAllProducts() throws HibernateException;

    public void removeProduct(Integer id) throws HibernateException;

    public Product getOneProductById(Integer id);

    public List<Product> getSeveralProducts(Integer pageNumber, Integer pageSize);

    public BigInteger getCountRecords();

    public List<Product> getSeveralProducts(Integer[] idList);
}
