package com.manager;


import com.model.Product;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by Fedoroff.
 * User: Orion
 * Date: 12.08.12
 * Time: 12:25
 */

public interface ProductManager {
    Product getProductById(int id);
    void saveProduct(Product product);
    List<Product> getAllProducts();
    public void removeProduct(Integer id);
    public Product getOneProductById(Integer id);
    public List<Product> getPageOfProducts(Integer pageNumber,Integer pageSize);
    public BigInteger getCountRecords();
    public List<Product> getSeveralProducts(Integer[] idList);
}
