package com.manager.impl;

import com.dao.UserDao;
import com.manager.UserManager;
import com.model.Basket;
import com.model.User;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Артем
 * Date: 16.05.14
 * Time: 14:56
 * By Orion
 */
@Service("userManager")
public class UserManagerImpl implements UserManager {
    @Autowired
    UserDao userDao;
    public List<User> getJustUsers() throws HibernateException{
        return userDao.getJustUsers();
    }
    @Override
    public void saveUser(User user) throws HibernateException {
        userDao.saveUser(user);
    }

    @Override
    public void removeUser(User user) throws HibernateException {
       userDao.removeUser(user);
    }

    @Override
    public List<User> getAllUsers() throws HibernateException {
        return userDao.getAllUsers();
    }

    @Override
    public User getUserByLogin(String login) throws HibernateException {
        return userDao.getUserByLogin(login);
    }

    @Override
    public List<Basket> getBasketOfUser(User user) throws HibernateException {
        return userDao.getBasketOfUser(user);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void saveBasketOfUser(User user) throws HibernateException {
        userDao.saveBasketOfUser(user);
        //To change body of implemented methods use File | Settings | File Templates.
    }



}
