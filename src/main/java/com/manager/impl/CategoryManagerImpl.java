package com.manager.impl;

import com.dao.CategoryDao;
import com.model.Category;
import com.manager.CategoryManager;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Fedoroff.
 * User: Orion
 * Date: 05.08.12
 * Time: 21:06
 */
@Service("categoryManager")
public class CategoryManagerImpl implements CategoryManager {

    @Autowired
    private CategoryDao categoryDao;
    @Override
    public List<Category> getAllCategories(){
        return categoryDao.getAllCategories();
    }
    @Override
    public Category getOneCategoryById(Integer id){
        return categoryDao.getOneCategoryById(id);
    }
    @Override
    public Category getCategoryById(int id)
    {
        return categoryDao.getCategoryById(id);
    }
    @Override
    public void saveCategory(Category category)
    {
        categoryDao.saveCategory(category);
    }

    @Override
    public void removeCategory(Integer id) throws HibernateException {
        categoryDao.removeCategory(id);
    }

    public CategoryDao getCategoryDAO() {
        return categoryDao;
    }
    @Override
    public List<Category> getSeveralCategories(Integer[] idList){
        return categoryDao.getSeveralCategories(idList);
    }
}
