package com.manager.impl;

import com.dao.ProductDao;
import com.model.Product;
import com.manager.ProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by Fedoroff.
 * User: Orion
 * Date: 12.08.12
 * Time: 12:27
 */
@Service("productManager")
public class ProductManagerImpl implements ProductManager {

   @Autowired
    private ProductDao productDao;

    @Override
    public List<Product> getAllProducts(){
        return productDao.getAllProducts();
    }
    @Override
    public Product getProductById(int id) {
        return productDao.getProductById(id);
    }
    @Override
    public Product getOneProductById(Integer id){
        return productDao.getOneProductById(id);
    }
    public void setProductDAO(ProductDao productDAO){
        this.productDao = productDao;
    }
    @Override
    public void saveProduct(Product product) {
        productDao.saveProduct(product);
    }
    public ProductDao getProductDAO(){
        return productDao;
    }
    @Override
    public List<Product> getPageOfProducts(Integer pageNumber,Integer pageSize){
        Integer lastId=pageNumber*pageSize;//индекс первого продукта с выборки
        Integer firstId=pageNumber*pageSize-pageSize;//индекс последнего продукта с выборки
        return productDao.getSeveralProducts(firstId,lastId);
    }
    @Override
    public void removeProduct(Integer id){
        productDao.removeProduct(id);
    }
    @Override
    public BigInteger getCountRecords(){
        return productDao.getCountRecords();
    }
    @Override
    public List<Product> getSeveralProducts(Integer[] idList){
       return productDao.getSeveralProducts(idList);
    }
}
