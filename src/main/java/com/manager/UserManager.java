package com.manager;

import com.model.Basket;
import com.model.User;
import org.hibernate.HibernateException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Артем
 * Date: 16.05.14
 * Time: 14:56
 * By Orion
 */
public interface UserManager {
    public void saveUser(User user) throws HibernateException;
    public void removeUser(User user) throws HibernateException;
    public List<User> getAllUsers() throws HibernateException;
    public User getUserByLogin(String login)throws HibernateException;
    public List<Basket> getBasketOfUser(User user) throws HibernateException;
    public void saveBasketOfUser(User user) throws HibernateException;
    public List<User> getJustUsers() throws HibernateException;
}
