package com.manager;

import com.model.Category;
import org.hibernate.HibernateException;
import java.util.List;

/**
 * Created by Fedoroff.
 * User: Orion
 * Date: 05.08.12
 * Time: 21:14
 */

public interface CategoryManager {
    Category getCategoryById(int id);

    public Category getOneCategoryById(Integer id);

    void saveCategory(Category category);

    public void removeCategory(Integer id) throws HibernateException;

    List<Category> getAllCategories();

    List<Category> getSeveralCategories(Integer[] idList);
}
