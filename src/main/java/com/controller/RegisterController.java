package com.controller;

import com.manager.UserManager;
import com.model.AccessLevel;
import com.model.User;
import com.model.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created with IntelliJ IDEA.
 * User: Артем
 * Date: 16.05.14
 * Time: 10:13
 * By Orion
 */
@Controller
public class RegisterController {
    @Autowired
    UserManager userManager;
    @Autowired
    UserValidator userValidator;
    public void setValidator(UserValidator userValidator) {
        this.userValidator = userValidator;
    }
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView regMe(@ModelAttribute(value = "user") User user, BindingResult result) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/login");
        userValidator.validate(user,result);
        if (result.hasErrors()){
            return new ModelAndView("/reg","user",user);
        }
        user.setEnabled(true);
        user.setAccessLevel(AccessLevel.ROLE_USER);
        userManager.saveUser(user);
        return modelAndView;
    }

    @RequestMapping(value = "/reg", method = RequestMethod.GET)
    public ModelAndView requestReg() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", new User());
        return modelAndView;
    }
}
