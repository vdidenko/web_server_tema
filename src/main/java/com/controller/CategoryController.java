package com.controller;

import com.manager.CategoryManager;
import com.model.Category;
import com.model.validator.CategoryValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by IntelliJ IDEA.
 * User: Orion
 * Date: 05.04.13
 * Time: 19:18
 * That which we call a rose by any other name would smell as sweet.
 *
 * @author Artem FedoroFF
 */
@Controller
public class CategoryController {
    @Autowired
    private CategoryManager categoryManager;
    @Autowired
    private CategoryValidator categoryValidator;

    @RequestMapping(value = "/logined/category", method = RequestMethod.GET)
    public ModelAndView requesting() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("category", new Category());
        return modelAndView;
    }

    public void setValidator(CategoryValidator categoryValidator) {
        this.categoryValidator = categoryValidator;
    }

    @RequestMapping(value = "/logined/category/add", method = RequestMethod.POST)
    public ModelAndView setListOfCategories(@ModelAttribute("category") Category category, BindingResult result) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/logined/category");
        categoryValidator.validate(category, result);
        if (result.hasErrors()) {
            return new ModelAndView("/logined/category", "category", category);
        }
        categoryManager.saveCategory(category);
        modelAndView.addObject("category_list", categoryManager.getAllCategories());
        return modelAndView;
    }

    @RequestMapping(value = "/logined/category/delete", method = RequestMethod.GET)

    public ModelAndView delCategory(@RequestParam("id") String id) {
        categoryManager.removeCategory(Integer.decode(id));
        ModelAndView model = new ModelAndView();
        model.setViewName("/logined/successDeleteCategory");
        model.addObject("category_list", categoryManager.getAllCategories());
        return model;
    }

    @RequestMapping(value = "/logined/viewer/categories", method = RequestMethod.GET)
    public ModelAndView getListOfCategories() {
        ModelAndView model = new ModelAndView();
        model.setViewName("/logined/viewer");
        model.addObject("category_list", categoryManager.getAllCategories());
        return model;
    }

    @RequestMapping(value = "/logined/fullCategoryInf", method = RequestMethod.GET)
    public ModelAndView getListOfProducts(@RequestParam("id") String id) {
        ModelAndView model = new ModelAndView();
        model.addObject("products_list", categoryManager.getCategoryById(Integer.decode(id)).getProducts());
        model.addObject("categoryView", categoryManager.getOneCategoryById(Integer.decode(id)));
        return model;
    }
}
