package com.controller;

import com.manager.UserManager;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: TEMA
 * Date: 12.06.14
 * Time: 0:32
 * that which we call a rose by any other name would smell as sweet
 */
@Controller
public class UserController {
    @Autowired
    private UserManager userManager;

    @RequestMapping(value = "/logined/displayUsers", method = RequestMethod.GET)
    public ModelAndView displayUsers() {
        ModelAndView model = new ModelAndView();
        ArrayList<User> userList = new ArrayList<User>();
        userList = (ArrayList<User>) userManager.getJustUsers();
        model.addObject("userList", userList);
        return model;
    }

    @RequestMapping(value = "/logined/deleteUser", method = RequestMethod.GET)
    public ModelAndView reqDelUser(@RequestParam("id") String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/logined/displayUsers");
        userManager.removeUser(userManager.getUserByLogin(id));
        return modelAndView;
    }

    @RequestMapping(value = "/logined/disableUser", method = RequestMethod.GET)
    public ModelAndView reqDisUser(@RequestParam("id") String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/logined/displayUsers");
        User user = new User();
        user = userManager.getUserByLogin(id);
        if (user.isEnabled()) {
            user.setEnabled(false);
        } else {
            user.setEnabled(true);
        }
        userManager.saveUser(user);
        return modelAndView;
    }

}
