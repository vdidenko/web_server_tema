package com.controller;

import com.manager.CategoryManager;
import com.manager.ProductManager;
import com.model.Product;
import com.model.validator.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Orion
 * Date: 05.04.13
 * Time: 19:47
 * That which we call a rose by any other name would smell as sweet.
 *
 * @author Artem FedoroFF
 */
@Controller
public class ProductController {
    @Autowired
    ProductValidator productValidator;
    @Autowired
    CategoryManager categoryManager;
    @Autowired
    private ProductManager productManager;

    public void setValidator(ProductValidator productValidator) {
        this.productValidator = productValidator;
    }

    @RequestMapping(value = "/logined/product", method = RequestMethod.GET)
    public ModelAndView requestProduct() {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("product", new Product());
        modelAndView.addObject("category_list", categoryManager.getAllCategories());
        return modelAndView;
    }

    @RequestMapping(value = "/logined/product/add", method = RequestMethod.POST)
    public ModelAndView setListOfProducts(@ModelAttribute("product") Product product, @RequestParam(value = "idList[]") Integer[] idList, BindingResult result) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/logined/product");
        productValidator.validate(product, result);
        if (result.hasErrors()) {
            return new ModelAndView("/logined/product", "product", product);
        }
        if (null != idList) {
            product.setCategories(categoryManager.getSeveralCategories(idList));
            productManager.saveProduct(product);
            modelAndView.addObject("product_list", productManager.getAllProducts());
        }
        return modelAndView;
    }

    @RequestMapping(value = "/logined/viewer", method = RequestMethod.GET)
    public void requestView() {
    }

    @RequestMapping(value = "/logined/viewer/products", method = RequestMethod.GET)
    public ModelAndView getListOfProducts(@RequestParam("pageNumber") Integer pageNumber) {
        ModelAndView model = new ModelAndView();
        model.setViewName("/logined/viewer");
        List<Integer> pages = new ArrayList<Integer>();
        //размер страницы задается статически в методе контроллера (пользователь не может его изменить)
        Integer pageSize = 10;
        //вычисление количества страниц учитывая размер страницы
        Integer countOfPages = productManager.getCountRecords().intValue() / pageSize;
        if (productManager.getCountRecords().intValue() % pageSize != 0) {
            countOfPages++;
        }

        //инициализация постраничного вывода продуктов
        for (int i = 1; i <= countOfPages; i++) {
            pages.add(i);
        }
        model.addObject("product_list", productManager.getPageOfProducts(pageNumber, pageSize));
        model.addObject("pages", pages);
        model.addObject("pageN", pageNumber);
        return model;
    }

    @RequestMapping(value = "/logined/product/delete", method = RequestMethod.GET)
    public ModelAndView delProduct(@RequestParam("id") Integer id) {
        productManager.removeProduct(id);
        ModelAndView model = new ModelAndView();
        model.setViewName("/logined/successDeleteProduct");
        model.addObject("product_list", productManager.getAllProducts());
        return model;
    }
}
