package com.controller;

import com.manager.ProductManager;
import com.manager.UserManager;
import com.model.Basket;
import com.model.User;
import com.model.validator.BasketValidator;
import com.model.validator.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: TEMA
 * Date: 24.05.14
 * Time: 16:05
 * To change this template use File | Settings | File Templates.
 */
@Controller
@SessionAttributes(types = Basket.class)
public class BasketController {
    @Autowired
    private UserManager userManager;
    @Autowired
    private ProductManager productManager;
    @Autowired
    private BasketValidator basketValidator;
    public void setValidator(BasketValidator basketValidator) {
        this.basketValidator = basketValidator;
    }
    @RequestMapping(value = "/logined/addBasket", method = RequestMethod.POST)
    public ModelAndView setBaskets(@ModelAttribute("basket") Basket basket, SessionStatus sessionStatus, BindingResult result) {
        ModelAndView model = new ModelAndView();
        model.setViewName("/logined/viewer");
        basketValidator.validate(basket,result);
        if (result.hasErrors()) {
            return model;
        }
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User user = new User();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof UserDetails) {
                user = userManager.getUserByLogin(((UserDetails) principal).getUsername());
            }
        }
        if (basket != null) {
            ArrayList<Basket> baskets = new ArrayList<Basket>();
            basket.setUser(user);
            baskets.add(basket);
            user.setBasketList(baskets);
            userManager.saveBasketOfUser(user);
        }
        sessionStatus.setComplete();
        return model;
    }

    @RequestMapping(value = "/logined/fullProductInf", method = RequestMethod.GET)
    public ModelAndView reqBasket(@RequestParam("id") String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("product", productManager.getOneProductById(Integer.decode(id)));
        Basket basket = new Basket();
        basket.setProduct(productManager.getOneProductById(Integer.decode(id)));
        modelAndView.addObject("basket", basket);
        return modelAndView;
    }

    @RequestMapping(value = "/logined/viewer/basket", method = RequestMethod.GET)
    public ModelAndView getListOfProducts() {
        ModelAndView model = new ModelAndView();
        model.setViewName("/logined/viewer");
        List<Basket> basketList = new ArrayList<Basket>();
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User user = new User();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof UserDetails) {
                user = userManager.getUserByLogin(((UserDetails) principal).getUsername());
            }
        }
        basketList = userManager.getBasketOfUser(user);
        model.addObject("basketList", basketList);
        return model;
    }
}
