package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * User: HOME
 * Date: 30.03.13
 * Time: 15:54
 */
@Controller
public class IndexController {
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public void indexRequest() {
    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public void aboutRequest() {

    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView noPath() {
        ModelAndView view = new ModelAndView();
        view.setViewName("index");
        return view;
    }
}
