package com;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by IntelliJ IDEA.
 * User: Orion
 * Date: 24.03.13
 * Time: 20:28
 * That which we call a rose by any other name would smell as sweet.
 *
 * @author Artem FedoroFF
 */

public class SessionManager {
    @Autowired
    private SessionFactory sessionFactory;


    public Session getCurrentSession() throws HibernateException {
        return sessionFactory.getCurrentSession();
    }
}
